import { Navigate, createBrowserRouter } from "react-router-dom";
import IndexMedecin from "../layouts/MedecinLayout";
import HomeMedecin from "../views/HomeMedecin";
import MedecinLoginPatient from "../views/MedecinLoginPatient";
import DefaultLayout from "../layouts/DefaultLayout";
import { InscriptionMedecin } from "../views/inscription";
import LoginCard from "../components/LoginForAll";
import NotFound from "../components/NotFound";

// import NotFound from "../componants/NotFound";

// import MedecinLoginPatient from "../views/medecin-views/MedecinLoginPatient";

// import MedecinDashboard from "../views/medecin-views/dashboard-medecin";
// import MedecinHistorique from "../views/medecin-views/historique-medecin";


// import ConsultationMedecin from "../views/medecin-views/Consultation";
// import MedecinProfile from "../views/medecin-views/Profile-medecin";
// import MedecinPreferences from "../views/medecin-views/preferences-medecin";
// import HomeMedecin from "../views/HomeMedecin";




const router = createBrowserRouter([
    {
        path:'/',
        element:<DefaultLayout />,
        children:[
            {
                path: '/',
                element: <Navigate to={'/login'}/>
            },
            {
                path: '/login',
                element: <LoginCard action="/inscription/medecin" image="medecin_connect.png" logo="logo.png" direction={true} inscription={true} theme="primary" />

            },
            {
                path: '/inscription/medecin',
                element: <InscriptionMedecin/>
            },

    ]
    },


    {
        path: '/medecin',
        element: <IndexMedecin/>,
        children:[
            {
                path: '/medecin',
                element: <Navigate to={'/medecin/home-medecin'}/>
            },
            {
                path: '/medecin/home-medecin',
                element: <HomeMedecin/>
            },
            // {
            //     path: '/medecin/profile',
            //     element: <MedecinProfile/>
            // },

            {
                path: '/medecin/patient',
                element: <MedecinLoginPatient/>,
            },

            // {
            //     path: '/medecin/dashboard',
            //     element: <MedecinDashboard/>
            // },
            // {
            //     path: '/medecin/preferences',
            //     element: <MedecinPreferences/>
            // }
            // ,
            // {
            //     path: '/medecin/historique',
            //     element: <MedecinHistorique />
            // },
            // {
            //     path: '/medecin/consultation',
            //     element: <ConsultationMedecin />
            // }


        ]
    },

    // {
    //     path: '/patient',
    //     element: <IndexPatient/>,
    //     children:[
    //         {
    //             path: '/patient',
    //             element: <Navigate to={'/patient/home'}/>
    //         },
    //         {
    //             path: '/patient/login',
    //             element: <PatientViews.P_Login/>
    //         },
    //         {
    //             path: '/patient/profile',
    //             element: <PatientViews.P_Profile/>
    //         },
    //         {
    //             path: '/patient/home',
    //             element: <PatientViews.P_Home/>
    //         },
    //         {
    //             path: '/patient/examens',
    //             element: <PatientViews.P_Examens/>
    //         }
    //         ,
    //         {
    //             path: '/patient/vaccination',
    //             element: <PatientViews.P_Vaccination />
    //         }
    //         ,
    //         {
    //             path: '/patient/courbes',
    //             element: <PatientViews.P_Courbes />
    //         }

    //         ,
    //         {
    //             path: '/patient/hospitalisation',
    //             element: <PatientViews.P_Hospitalisation />
    //         }

    //         ,
    //         {
    //             path: '/patient/preferences',
    //             element: <PatientViews.P_Preferences />
    //         }

    //         ,
    //         {
    //             path: '/patient/famille',
    //             element: <PatientViews.P_Famille />
    //         }
    //         ,
    //         {
    //             path: '/patient/informations-general',
    //             element: <PatientViews.P_Informations />
    //         }

    //         ,
    //         {
    //             path: '/patient/consultations',
    //             element: <PatientViews.P_Consultations />
    //         }


    //     ]
    // },
    {
        path: '*',
        element: <NotFound/>
    },
]);

export default router;
