import React from 'react';
import { Link, Outlet } from 'react-router-dom';

const DefaultLayout = () => {
  return (
    <div className="flex flex-col min-h-screen">
      {/* Header */}
      <header className="bg-blue-600 text-white p-4">
        <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8 flex justify-between items-center">
          <h1 className="text-2xl font-bold">My Application</h1>
          <nav>
            <Link href="/" className="text-white px-3 py-2 rounded-md text-sm font-medium">Home</Link>
            <Link href="/about" className="text-white px-3 py-2 rounded-md text-sm font-medium">About</Link>
            <Link href="/contact" className="text-white px-3 py-2 rounded-md text-sm font-medium">Contact</Link>
          </nav>
        </div>
      </header>

      {/* Main Content */}
      <main className="flex-grow">
        <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8 py-8">
          <Outlet />
        </div>
      </main>

      {/* Footer */}
      <footer className="bg-gray-800 text-white p-4">
        <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8 text-center">
          <p>&copy; {new Date().getFullYear()} My Application. All rights reserved.</p>
        </div>
      </footer>
    </div>
  );
};

export default DefaultLayout;
