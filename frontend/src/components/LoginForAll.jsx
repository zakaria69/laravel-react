import React, { useState } from 'react';
import { Link, useNavigate } from "react-router-dom";
import { axiosClient } from '../api/axios';

const LoginCard = (props) => {
  const [email, setEmail] = useState('zakaria@gmail.com');
  const [password, setPassword] = useState('12345678');
  const [errors, setErrors] = useState({ email: '', password: '' });

  const navigate = useNavigate()

  const validateForm = () => {
    let isValid = true;
    let errors = {};

    if (!email) {
      errors.email = 'Votre Identifiant est requis';
      isValid = false;
    }

    if (!password) {
      errors.password = 'Mot de passe est requis';
      isValid = false;
    } else if (password.length < 8) {
        errors.password = 'Mot de passe doit être au moins 8 caractères';
        isValid = false;
    }

    setErrors(errors);
    return isValid;
  };

//   const handleSubmit = async (e) => {
//     e.preventDefault();
//     if (validateForm()) {
//       try {
//         console.log('Fetching CSRF token...');
//         await axiosClient.get('/sanctum/csrf-cookie');
//         console.log('CSRF token fetched successfully');

//         const payload = { email, password };
//         console.log('Sending login request...', payload);
//         const response = await axiosClient.post('/login', payload);

//         console.log('Response received:', response);

//         // Assuming the API returns a success field or similar
//         if (response.status === 204 || (response.status === 200 && response.data.success)) {
//           navigate('/medecin');
//         } else {
//           // Handle unsuccessful login attempt
//           console.log('Login failed:', response);
//           setErrors({ ...errors, general: 'Invalid login credentials' });
//         }
//       } catch (error) {
//         console.error('There was an error:', error);
//         setErrors({ ...errors, general: 'An error occurred during login. Please try again.' });
//       }
//     }
//   };



  const handleSubmit = async (e) => {


    e.preventDefault();
    if (validateForm()) {


        await axiosClient.get('/sanctum/csrf-cookie')

        let payload ={email,password}
        const data = axiosClient.post('/login' ,payload)
        .then(()=>{
            console.log(data);

            if(data.state === 204){
                navigate('/medecin/home-medecin')
            }
        })
        .catch((error)=>{
            console.log('there is an error' ,error)
        })



    }
  };



  const handleLogout = async () => {
    try {
      // Send a logout request to your backend (if needed)
      await axiosClient.post('/logout'); // Adjust the URL as per your backend endpoint

      // Clear any client-side stored data (if needed)
      // localStorage.clear(); // For example, clear local storage

      // Redirect the user to the login page

    } catch (error) {
      console.error('Logout failed:', error);
      // Handle logout failure
    }
  };

  return (
    <div className="">
<button onClick={handleLogout}>Logout</button>
      <div className={`shadow-xl gap-5 rounded-3xl bg-white w-3/4 m-auto my-5 min-h-full justify-center px-6 py-12 lg:px-8 flex flex-wrap row${!props.direction ? "flex-row-reverse" : ""}`}>
        <div className="col-md-6">
          <div className="sm:mx-auto sm:w-full sm:max-w-sm">
            <img
              className="mx-auto h-16 w-auto object-cover"
              src={`/src/assets/img/${props.logo}`}
              alt="Your Company"
            />
            <h2 className="mt-10 text-center text-2xl font-bold leading-9 tracking-tight text-gray-900">
              {/* Add title here if needed */}
            </h2>
          </div>

          <div className="mt-10 sm:mx-auto sm:w-full sm:max-w-sm">
            <form className="space-y-6 " action={props.action} method="POST" onSubmit={handleSubmit}>
              <div>
                <label htmlFor="email"
                 className={`block text-sm font-medium leading-6 ${errors.email ? "text-red-500" : "text-gray-900" }`}>
                  Votre Identifiant
                </label>
                <div className="mt-2">
                  <input
                    id="email"
                    name="email"
                    type="email"

                    placeholder="ID"
                    className="bg-gray-100 px-2 block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 sm:text-sm sm:leading-6 outline-none"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                  />
                  {errors.email && <p className="text-red-500 text-xs mt-1">{errors.email}</p>}
                </div>
              </div>

              <div>
                <div className="flex items-center justify-between">
                  <label htmlFor="password"
                  className={`block text-sm font-medium leading-6 ${errors.password ? "text-red-500" : "text-gray-900" }`}>
                    Mot de passe
                  </label>
                  <div className="text-sm">
                    <Link to="#" className={`font-semibold text-${props.theme}-color`}>
                      Mot de passe oublié ?
                    </Link>
                  </div>
                </div>
                <div className="mt-2">
                  <input
                    id="password"
                    name="password"
                    type="password"

                    placeholder="Votre mot de passe"
                    className="bg-gray-100 px-2 block w-full rounded-md border-0 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 sm:text-sm sm:leading-6 outline-none"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                  />
                  {errors.password && <p className="text-red-500 text-xs mt-1">{errors.password}</p>}
                </div>
              </div>

              <div>
                <button
                  type="submit"
                  className={`flex w-full justify-center rounded-md bg-${props.theme}-color px-3 py-1.5 text-sm font-semibold leading-6 text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-secondary-color`}
                >
                  Se connecter
                </button>
              </div>
            </form>

            {props.inscription ? (
              <p className="mt-10 text-center text-sm text-gray-500">
                Vous n'avez pas de compte ?{' '}
                <Link to={props.action} className={`font-semibold leading-6 text-${props.theme}-color`}>
                  S'inscrire
                </Link>
              </p>
            ) : (
              ""
            )}
          </div>
        </div>
        <div className="col-md-4 image items-center flex align-items-center">
          <img src={`/src/assets/img/${props.image}`} alt="image" className="w-full" />
        </div>
      </div>
    </div>
  );
};

export default LoginCard;

